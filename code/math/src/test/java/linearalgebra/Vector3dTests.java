// 2232311 || Jaypee Tello
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests {
    private Vector3d test1 = new Vector3d(1, 1, 2);
    private Vector3d test2 = new Vector3d(2, 3, 4);
    private Vector3d test3 = new Vector3d(3, 4, 6);
    private final double TOLERANCE = 0.0001;

    @Test
    public void shouldGetX() {
        assertEquals(1, test1.getX(), TOLERANCE);
    }

    @Test
    public void shouldGetY() {
        assertEquals(1, test1.getY(), TOLERANCE);
    }

    @Test
    public void shouldGetZ() {
        assertEquals(2, test1.getZ(), TOLERANCE);
    }

    @Test
    public void shouldGetMagnitude() {
        assertEquals(2.449489742783178, test1.magnitude(), TOLERANCE);
    }

    @Test 
    public void shouldGet13() {
        assertEquals(13, test1.dotProduct(test2), TOLERANCE);
    }

    @Test
    public void shouldGet346() {
        Vector3d addTest = test1.add(test2);

        assertEquals(test3.getX(), addTest.getX(), TOLERANCE);
        assertEquals(test3.getY(), addTest.getY(), TOLERANCE);
        assertEquals(test3.getZ(), addTest.getZ(), TOLERANCE);
    }
}
