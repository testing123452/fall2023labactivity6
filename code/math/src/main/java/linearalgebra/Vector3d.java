// Jaypee Tello || 2232311
package linearalgebra;

/**
 * Vector3d is a class to do some vectors
 * @author Jaypee Tello
 * @version 10/2/2023
 */

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d (double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    /**
     * returns x
     * @param x The value x
     * @return The value x
     */
    public double getX() {
        return this.x;
    }
    
    /**
     * returns y
     * @param y The value y
     * @return The value y
     */
    public double getY() {
        return this.y;
    }

    /**
     * returns z
     * @param x The value z
     * @return The value z
     */
    public double getZ() {
        return this.z;
    }

    /**
     * returns the magnitude
     * @return The magnitude number
     */
    public double magnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * returns the product of dot
     * @param Vector3dInput The vector 3d input
     * @return The product of dot, which is x, y ,z * vector3dinput
     */
    public double dotProduct(Vector3d Vector3dInput) {
        double dotX = Vector3dInput.getX();
        double dotY = Vector3dInput.getY();
        double dotZ = Vector3dInput.getZ();

        return ((this.x * dotX) + (this.y * dotY) + (this.z * dotZ));
    }

    /**
     * adds the vectors
     * @param Vector3dInput The value of a 3d vector
     */
    public Vector3d add(Vector3d Vector3dInput) {
        double inputX = Vector3dInput.getX();
        double inputY = Vector3dInput.getY();
        double inputZ = Vector3dInput.getZ();

        double newX = this.x  + inputX;
        double newY = this.y + inputY;
        double newZ = this.z + inputZ;

        Vector3d newVector3d = new Vector3d(newX, newY, newZ);

        return newVector3d;
    }

    /**
     * returns the String overrided
     * @return A string
     */
    public String toString() {
        return ("(" + this.x + "," + this.y + "," + this.z + ")");
    }
}
